#!/bin/sh -ex

# SPDX-FileCopyrightText: 2016 Pander <pander@users.sourceforge.net>
# SPDX-FileCopyrightText: 2024 linsui <linsui@inbox.lv>
# SPDX-License-Identifier: GPL-3.0-or-later

if [ $(flatpak list|grep org.inkscape.Inkscape|wc -l) -gt 0 ]; then
    inkscape=$(echo flatpak run org.inkscape.Inkscape)
else
    inkscape=$(echo inkscape)
fi

for i in *.svg; do
    png=`basename $i svg`png
    $inkscape $i -Co $png 2>&1 >/dev/null
    if [ "$CI_COMMIT_BRANCH" = "master" ]; then
        zopflipng --iterations=5 --filters=01234mepb --lossy_8bit --lossy_transparent -y $png $png.zopfli.png
        mv $png.zopfli.png $png
    fi
done
